//
//  ExtendedViewController.swift
//  Privy Technical
//
//  Created by Yogi Priyo Prayogo on 05/12/22.
//

import UIKit

extension UIViewController {
    func displayErrorAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

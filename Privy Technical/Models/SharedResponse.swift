//
//  SharedResponse.swift
//  Privy Technical
//
//  Created by Yogi Priyo Prayogo on 05/12/22.
//

struct BaseResponse<T : Codable>: Codable {
    let data: T
}

//
//  User.swift
//  Privy Technical
//
//  Created by Yogi Priyo Prayogo on 05/12/22.
//

struct User: Codable {
    let user: UserDetails
}

struct UserDetails: Codable {
    let id: String
    let phone: String
    let userStatus: String
    let userType: String
    let sugarId: String
    let country: String
    let latlong: String? = nil
    let userDevice: UserDevice
    
    enum CodingKeys: String, CodingKey {
        case id, phone
        case userStatus = "user_status"
        case userType = "user_type"
        case sugarId = "sugar_id"
        case country
        case latlong
        case userDevice = "user_device"
    }
}

struct UserDevice: Codable {
    let deviceToken: String
    let deviceType: String
    let deviceStatus: String
    
    enum CodingKeys: String, CodingKey {
        case deviceToken = "device_token"
        case deviceType = "device_type"
        case deviceStatus = "device_status"
    }
}

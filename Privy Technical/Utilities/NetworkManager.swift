//
//  NetworkManager.swift
//  Privy Technical
//
//  Created by Yogi Priyo Prayogo on 05/12/22.
//

import Foundation

struct NetworkManager {
    enum DestinationHost: String {
        case register
        case otpRequest = "otp/request"
        case otpMatch = "otp/match"
    }
    
    let rootHost: String = "http://pretest-qa.dcidev.id/api/v1/"
    
    func sendData(destination: DestinationHost,
                   moreParams: [String: Any]? = nil,
                   completionHandler: @escaping (Data) -> Void,
                   errorHandler: @escaping () -> Void
    ) {
        guard let components = URLComponents(string: rootHost+destination.rawValue) else { return }
        guard let url = components.url else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        guard let moreParams = moreParams, let httpBody = try? JSONSerialization.data(withJSONObject: moreParams, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error {
                print("Error with fetching data: \(error)")
                errorHandler()
                return
            }

            guard let httpResponse = response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else {
                print("Error with the response, unexpected status code: \(String(describing: response))")
                errorHandler()
                return
            }
            
            if let data = data {
                completionHandler(data)
            }
        })
        
        task.resume()
    }
}

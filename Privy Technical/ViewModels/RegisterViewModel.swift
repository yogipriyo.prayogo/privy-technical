//
//  RegisterViewModel.swift
//  Privy Technical
//
//  Created by Yogi Priyo Prayogo on 05/12/22.
//

import Foundation

protocol RegisterViewModelDelegate {
    func receiveData(userDetails: UserDetails)
    func receiveError()
}

struct RegisterViewModel {
    var delegate: RegisterViewModelDelegate?
    
    func registerUser(phone: String, password: String, country: String, deviceToken: String) {
        let moreParams: [String: Any] = [
            "phone": phone,
            "password": password,
            "country": country,
            "latlong": "0",
            "device_token": deviceToken,
            "device_type": 0
        ]
        
        NetworkManager().sendData(destination: .register, moreParams: moreParams) { data in
            if let baseResponse = try? JSONDecoder().decode(BaseResponse<User>.self, from: data) {
                delegate?.receiveData(userDetails: baseResponse.data.user)
            }
        } errorHandler: {
            delegate?.receiveError()
        }
    }
}

//
//  LoginViewController.swift
//  Privy Technical
//
//  Created by Yogi Priyo Prayogo on 02/12/22.
//

import UIKit

final class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginFormContainerView: UIView!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func registerTapped(_ sender: UIButton) {
        
    }
    
}

//
//  OtpViewController.swift
//  Privy Technical
//
//  Created by Yogi Priyo Prayogo on 04/12/22.
//

import UIKit

final class OtpViewController: UIViewController {
    
    @IBOutlet weak var firstOtpTextField: UITextField!
    @IBOutlet weak var secondOtpTextField: UITextField!
    @IBOutlet weak var thirdOtpTextField: UITextField!
    @IBOutlet weak var fourthOtpTextField: UITextField!
    
    private let phoneNumber: String
    
    init(_ phoneNumber: String) {
        self.phoneNumber = phoneNumber
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func verifyOtpTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func resendOtpTapped(_ sender: UIButton) {
        
    }
    
}

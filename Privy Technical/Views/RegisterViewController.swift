//
//  RegisterViewController.swift
//  Privy Technical
//
//  Created by Yogi Priyo Prayogo on 03/12/22.
//

import UIKit

final class RegisterViewController: UIViewController, RegisterViewModelDelegate {
    
    @IBOutlet weak var registerFormContainerView: UIView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private var viewModel: RegisterViewModel = RegisterViewModel()
    private var phoneNumber: String = ""
    
    @IBOutlet weak var phoneTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.viewModel.delegate = self
    }
    
    fileprivate func verifyData() -> Bool {
        var verified: Bool = true
        
        let textFieldArray: [UITextField] = [passwordTextField, countryTextField, countryTextField]
        for textField in textFieldArray where !textField.hasText {
            verified = false
            break
        }
        
        return verified
    }
    
    @IBAction func registerTapped(_ sender: UIButton) {
        if verifyData() {
            self.activityIndicator.startAnimating()
            self.phoneNumber = phoneTextField.text ?? ""
            self.viewModel.registerUser(
                phone: phoneTextField.text ?? "",
                password: passwordTextField.text ?? "",
                country: countryTextField.text ?? "",
                deviceToken: UIDevice.current.identifierForVendor?.uuidString ?? ""
            )
        } else {
            self.displayErrorAlert(title: "Maaf!", message: "Silahkan isi semua kolom.")
        }
    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        let loginVC: LoginViewController = LoginViewController()
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    func receiveData(userDetails: UserDetails) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.activityIndicator.stopAnimating()
            
            let otpVC: OtpViewController = OtpViewController(self.phoneNumber)
            self.navigationController?.pushViewController(otpVC, animated: true)
        }
    }
    
    func receiveError() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator.stopAnimating()
            self?.displayErrorAlert(title: "Maaf!", message: "Terjadi kesalahan, silahkan ulangi.")
        }
    }
}
